mapDoubler1 :: Num a => [a] -> [a]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = 2*x : mapDoubler1 xs