import Data.Char

decaler :: Int -> Char -> Char
decaler n x = if isLetter x then chr (ord x + n) else x

chiffrerCesar :: Int -> String -> String
chiffrerCesar _ [] = []
chiffrerCesar n (x:xs) = decaler n x : chiffrerCesar n xs 

main :: IO ()
main = print(chiffrerCesar 1 "coucou")