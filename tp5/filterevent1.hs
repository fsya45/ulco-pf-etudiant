filterEven1::[Int]->[Int]
filterEven1 [] = []
filterEven1 (x:xs) = if even x then x:filterEven1 xs else filterEven1 xs