myRangeTuple1 :: [Int, Int] -> [Int]
myRangeTuple1 (x0, x1) = (x0 .. x1)

myRangecurry1 :: Int -> Int ->[Int]
myRangecurry1 x0 x1 = (x0 .. x1)

myRangeTuple2 :: Int -> [Int]
myRangeTuple2 x1 = myRangeTuple1(0, x1)

myRangecurry2 :: Int -> [Int]
myRangecurry2 = myRangecurry1 0
myCurry :: ((a, b) -> c) -> a -> b -> c
myCurry f x y = f(x, y)

main :: IO ()
main = do
    print(myRangecurry1 (0, 9))
    print(myRangeTuple1(0 9)
    print(myRangeTuple2 (9))
    print(myRangecurry2 9)
    print (myCurry (myRangeTuple2(9)))
