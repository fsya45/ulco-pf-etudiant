plus42Positif :: Int -> Bool
plus42Positif = (>0) . (+42)

main :: IO ()
main = do
    print (plus42Positif 2)
    print (plus42Positif (-84))