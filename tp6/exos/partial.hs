myTake2 :: [Int] -> [Int]
myTake2 = take 2 

myGet2 :: [Int] -> [Int]
myGet2  = Flip (!!) list 2

print (map (2*) [1..4])

main = do
    print (myTake2 [1..4])
    print (myGet2 [1..4])
