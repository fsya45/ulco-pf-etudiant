import System.Random

main :: IO ()
main = do
 number <- randomRIO(0, 100::Int)
 print number
