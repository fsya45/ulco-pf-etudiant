import System.Random

loop :: Int -> Int -> IO ()
loop t 0 = putStrLn "fin"
loop t n = do
    print t
    loop t (n-1)

main :: IO ()
main = do
    target <- randomRIO (1, 100)
    loop target 10
