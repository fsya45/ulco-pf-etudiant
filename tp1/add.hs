add2Int :: Int -> Int -> Int
add2Int x y = x + y



add2Double :: Double -> Double -> Double
add2Double x y =  x + y


add2Num :: Num a => a -> a -> a
add2Num x y = x + y

main :: IO ()
main = do
print(add2Int 2 3)
print(add2Double 2.0 3.0)
print(add2Num 2 3::Int)