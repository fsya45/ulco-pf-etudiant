import System.Environment

main :: IO ()
main = do
    args <- getArgs
    let text = unwords args
    putStrLn $ "  " ++ replicate (length text) '_'
    putStrLn $ "< " ++ text ++ " >"
    putStrLn $ "  " ++ replicate (length text) '-'
    putStrLn cow

cow :: String
cow = 
    "        \\   ^__^ \n\
    \         \\  (oo)\\_______ \n\
    \            (__)\\       )\\/\\ \n\
    \                ||----w | \n\
    \                ||     || "