fuzzyLength :: [a] -> String
fuzzyLength [] = "empty"
fuzzyLength [_] = "one"
fuzzyLength [_, _] = "two"
fuzzyLength _ = "many"