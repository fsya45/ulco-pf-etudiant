myFst :: (a, b) -> a
myFst (x, _) = x

mySnd :: (a, b) -> b
mySnd (_, y) = y

myFst3 :: (a, b, c) -> a
myFst3 (x, _, _) = x