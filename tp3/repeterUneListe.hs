twice :: [a] -> [a]
twice x = x ++ x

sym :: [a] -> [a]
sym x = x ++ (reverse x)