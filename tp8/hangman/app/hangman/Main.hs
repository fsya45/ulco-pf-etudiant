
import System.Random

 
gessinpendu :: [String]
gessinpendu = ["      ________       \n\
             \     |/       |      \n\
             \     |       (_)     \n\
             \     |       \\|/    \n\
             \     |        |      \n\
             \     |       / \\     \n\
             \_____|________________", 
             "      ________       \n\
             \     |/       |      \n\
             \     |       (_)     \n\
             \     |       \\|/    \n\
             \     |        |      \n\
             \     |               \n\
             \_____|________________",
             "      ________       \n\
             \     |/       |      \n\
             \     |       (_)     \n\
             \     |        |    \n\
             \     |        |      \n\
             \     |               \n\
             \_____|________________",
             "      ________       \n\
             \     |/       |      \n\
             \     |       (_)     \n\
             \     |              \n\
             \     |              \n\
             \     |               \n\
             \_____|________________",
             "      ________       \n\
             \     |/       |      \n\
             \     |              \n\
             \     |             \n\
             \     |              \n\
             \     |               \n\
             \_____|________________",
             "      ________       \n\
             \     |/             \n\
             \     |             \n\
             \     |             \n\
             \     |              \n\
             \     |               \n\
             \_____|________________",
             "             \n\
             \     |             \n\
             \     |            \n\
             \     |           \n\
             \     |              \n\
             \     |               \n\
             \_____|________________",
             "                   \n\
             \                  \n\
             \                   \n\
             \                   \n\
             \                   \n\
             \                    \n\
             \_____________________"]

marchestp :: [String]
marchestp = ["function", "functional programming", "haskell","list","pattern matching","recursion","tuple","type system"]


main :: IO ()
main = do 
  print $ marchestp
  putStrLn $ gessinpendu !! 5
  randomNumber <- randomRIO (1,8)
  putStrLn $ marchestp !! randomNumber