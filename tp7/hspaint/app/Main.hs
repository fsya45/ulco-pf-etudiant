{-# LANGUAGE OverloadedStrings #-}

import Data.IORef
import           GI.Cairo.Render
import           GI.Cairo.Render.Connector (renderWithContext)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.Gtk.Objects.Widget

type Coordonee = (Double, Double)
type Dessin = [Coordonee]

handleDraw :: IORef Dessin -> Gtk.DrawingArea -> Render Bool
handleDraw dessinRef canvas = do
    dessin <- liftIO $ readIORef dessinRef
    moveTo (fst (dessin !! 0)) (snd (dessin !!0))
    lineTo (fst (dessin !! 1)) (snd (dessin !!1))
    stroke
    return True

handlePress :: Gdk.EventButton -> IO Bool
handlePress b = do
    clickclak <- Gdk.getEventButtonButton b
    print clickclak
    return True

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    dessinRef <- newIORef [(50.0, 50.0), (100.0, 100.0)]

    canvas <- Gtk.drawingAreaNew
    _ <- Gtk.onWidgetDraw canvas $ renderWithContext $ handleDraw dessinRef canvas 
    Gtk.containerAdd window canvas 

    Gtk.widgetSetEvents canvas [ Gdk.EventMaskButtonPressMask ]
    _ <- onWidgetButtonPressEvent canvas handlePress 

    Gtk.widgetShowAll window
    Gtk.main

