fibo :: Int -> Int
fibo x =
    case x of
        0 -> 0
        1 -> 1
        _ -> fibo (x -2 ) + fibo (x - 1)

main :: IO ()
main = do
    print (fibo 9)
    print (fibo 10)
    print (fibo 11)