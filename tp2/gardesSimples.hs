formaterParite :: Int -> String
formaterParite x 
    | even x = "pair"
    | otherwise = "Impair"
    | x == 0 = "nul"
    
formaterSigne :: Int -> String
formaterSigne x
    | x == 0 = "nul"
    | x > 0 = "positif"
    | otherwise = "negatif"

main :: IO ()
main = do
    print (formaterParite (21))
    print (formaterParite (12))
    
    print (formaterSigne (-42))
    print (formaterSigne 42)
    print (formaterSigne 0)
