formateNul1 :: Int -> String
formateNul1 x =
    show x ++ " est " ++ nul x
    where
        nul n =
            case n of
            0 -> "nul"
            _ -> "non nul"

formateNul2 :: Int -> String
formateNul2 x =
    show x ++ " est " ++ nul x
    where
        nul 0 = "nul" 
        nul _ = "non nul"

main :: IO ()
main = do
    print (formateNul1 0)
    print (formateNul1 42)

    print (formateNul2 0)
    print (formateNul2 42)