borneEtFormate1::Double -> String
borneEtFormate1 x =
    show(x) ++ " -> " ++ show(y)
    where y =
        if x < 0 then
            0
        else if x > 1 then
            1
        else
            x

borneEtFormate2 :: Double -> String
borneEtFormate2 x =
    show(x) ++ " -> " ++ show(y)
    where y
        | x > 1 then 1
        | x < 0 then 0
        | otherwise x

main :: IO ()
main = do
    print (borneEtFormate1 (-1.2))
    print (borneEtFormate1 0.2)

    print (borneEtFormate2 (-1.2))
    print (borneEtFormate2 0.2)

