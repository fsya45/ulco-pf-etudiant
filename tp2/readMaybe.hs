import Text.Read

main :: IO ()
main = do
    line <- getLine
    let nb = readMaybe line :: Maybe Int
    print nb