import Text.Read

safeSqrt :: Double -> Maybe Double
safeSqrt x
    | x >= 0 = Just $ sqrt x
    | otherwise = Nothing

formatedSqrt :: Double -> String
formatedSqrt x = case safeSqrt x of
    Just n -> "sqrt(" ++ show x ++ ") = " ++ show n
    Nothing -> "sqrt(" ++ show x ++ ") is not defined"

main :: IO ()
main = do
    putStrLn $ formatedSqrt 16
    putStrLn $ formatedSqrt (-16)