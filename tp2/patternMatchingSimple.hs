fibo :: Int -> Int
fibo 0 = 0
fibo 1 = 1
fibo x = fibo (x - 2) + fibo (x - 1)

main :: IO ()
main = do
    print (fibo 9)
    print (fibo 10)
    print (fibo 11)