formaterParite :: Int -> String
formaterParite x = 
    if even x then 
        "pair"
    else
        "impair"

formaterSigne :: Int -> String
formaterSigne x =
    if x == 0 then
        "nul"
    else 
        if x < 0 then
            "négatif"
        else
            "positif"

main :: IO ()
main = do 
    print (formaterParite 21)
    print (formaterParite 12)
    
    print (formaterSigne (-42))
    print (formaterSigne 0)
    print (formaterSigne 42)



