import System.Environment
main :: IO ()
main = do
    args <- getArgs
    case args of
        ["naive", nStr] -> print $ fiboNaive $ read nStr
        ["tco", nStr] -> print $ fiboTco $ read nStr
        _ -> putStrLn "usage: <naive|tco> <n>"