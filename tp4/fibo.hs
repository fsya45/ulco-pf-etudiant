fiboNaive :: Int -> Int
fiboNaive 0 = 0
fiboNaive 1 = 1
fiboNaive n = fiboNaive (n-1) + fiboNaive (n-2)

fiboTco :: Int -> Int
fiboTco n = aux n 1 0
    where
        aux :: Int -> Int -> Int -> Int
        aux 0 _ pre = pre
        aux n cur pre = aux (n-1) (cur + pre) cur

main :: IO ()
main = do
    print $ fiboNaive 5
    print $ fiboNaive 10
    print $ fiboTco 5
    print $ fiboTco 10