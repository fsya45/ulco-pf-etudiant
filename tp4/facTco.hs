fact :: Int -> Int
fact 0 = 1
fact n = n * fact (n-1)

factTco :: Int -> Int
factTco n = aux n 1
    where
        aux 0 acc = acc
        aux n acc = aux (n-1) (n * acc)

main :: IO ()
main = do
    print (fact 2)
    print (fact 5)
    print (factTco 2)
    print (factTco 5)