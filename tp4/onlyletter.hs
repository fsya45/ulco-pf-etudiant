onlyLetters :: String -> String
onlyLetters "" = ""
onlyLetters (c:cs) = if isLetter c then c : onlyLetters cs else onlyLetters cs