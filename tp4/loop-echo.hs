loopEcho:: Int -> IO String
loopEcho 0 = do
    return "loop terminated"
loopEcho x = do
    putStr " > "
    msg <- getLine :: IO String
    if length msg == 0 then do
        return "empty line"
        else do
            putStrLn msg
            loopEcho (x-1)

main::IO()
main = do
    res <- loopEcho 3
    putStrLn res