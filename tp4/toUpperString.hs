toUpperString::[Char] -> [Char]
toUpperString [] = ""
toUpperString x = ([(toUpper $head x)] ++ (toUpperString  $drop 1 x))